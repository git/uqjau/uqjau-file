#!/bin/false -meant-to-be-sourced

# ====================================================================
# RCS INFORMATION (do not touch fields bounded by $)
# --------------------------------------------------------------------
#     $Date: 2013/08/17 18:12:27 $   (GMT)
# $Revision: 1.8 $
#   $Author: rodmant $ (aka last changed by)
#   $Locker:  $
#   $Source: /usr/local/7Rq/package/cur/file-2011.05.22/shar/lib/RCS/file_ls.perl.inc,v $
#      $Log: file_ls.perl.inc,v $
#      Revision 1.8  2013/08/17 18:12:27  rodmant
#      *** empty log message ***
#
#      Revision 1.5  2009/10/21 13:15:12  rodmant
#      *** empty log message ***
#
#      Revision 1.4  2009/08/23 00:09:02  rodmant
#      *** empty log message ***
#
# --------------------------------------------------------------------

# ====================================================================
# Copyright (c) Aug 2009, 2013 Tom Rodman <Rodman.T.S@gmail.com>
# ( The bulk for this script was created by running 'find2perl' )
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# --------------------------------------------------------------------

our @rwx = ('---','--x','-w-','-wx','r--','r-x','rw-','rwx');
our @moname = (Jan,Feb,Mar,Apr,May,Jun,Jul,Aug,Sep,Oct,Nov,Dec);

require "$_29lib/tsid.pl.func.inc";

# example: from ls_tsid_filter
#   ## main
#   while(<>)
#   {
#     chomp;
#     if (($dev,$ino,$mode,$nlink,$uid,$gid) = lstat($_)) {
#       ls({timestamp_type => "$timestamp_type"});
#     }else {
#       print STDERR "$ourname:WARNING: $_ [$!]";
#     }
#   }

our $sec_per_year = 365.25 * 24 * 60 * 60;

# calling perl script does this:
  #our ($dev,$ino,$mode,$nlink,$uid,$gid,$rdev,$sizemm,$atime,$mtime,$ctime,$blksize,$blocks);

sub ls {
  # --------------------------------------------------------------------
  # Synopsis: prepends a base 36
  #   integer timestamp to a fairly generic ls style output
  # --------------------------------------------------------------------
  # Rating: tone: tool used: often stable: y TBDs: y notable: y
  # TBD: make safe for 'use strict; use warnings;'
   
  my ($args_ref) = @_;

  ($dev,$ino,$mode,$nlink,$uid,$gid,$rdev,$sizemm,
    $atime,$mtime,$ctime,$blksize,$blocks) = lstat(_);
    # assume caller has already run lstat on the pathname

  if ( @_ )
  {
    eval qq{\$timestamp_type_ref = \\${$args_ref->{timestamp_type}}} ;
  }
  else
  {
    $timestamp_type_ref = \$mtime;
  }

  my $TSID = tsid( $$timestamp_type_ref ) ;

  my $pname = $name || $_;

  my $blocks;
  if (defined $blocks) {
      $blocks = int(($blocks + 1) / 2);
  }
  else {
      $blocks = int(($size + 1023) / 1024);
  }

  my $is_a_file=0;
  my $perms;
  if    (-f _) { $perms = '-'; $is_a_file=1;}
  elsif (-d _) { $perms = 'd'; }
  elsif (-c _) { $perms = 'c'; $sizemm = sizemm(); }
  elsif (-b _) { $perms = 'b'; $sizemm = sizemm(); }
  elsif (-p _) { $perms = 'p'; }
  elsif (-S _) { $perms = 's'; }
  else         { $perms = 'l'; $pname .= ' -> ' . readlink($_); }

  my $tmpmode = $mode;
  my $tmp = $rwx[$tmpmode & 7];
  $tmpmode >>= 3;
  $tmp = $rwx[$tmpmode & 7] . $tmp;
  $tmpmode >>= 3;
  $tmp = $rwx[$tmpmode & 7] . $tmp;
  substr($tmp,2,1) =~ tr/-x/Ss/ if -u _;
  substr($tmp,5,1) =~ tr/-x/Ss/ if -g _;
  substr($tmp,8,1) =~ tr/-x/Tt/ if -k _;
  $perms .= $tmp;

  #$user = $user{$uid} || $uid;
  #$group = $group{$gid} || $gid;

  ($sec,$min,$hour,$mday,$mon,$year) = localtime($$timestamp_type_ref);

  my $moname = $moname[$mon];

  my $timeyear;
  if ( (time - $$timestamp_type_ref)  > $sec_per_year / 2 ) {
      $timeyear = $year + 1900;
  }
  else {
      $timeyear = sprintf("%02d:%02d", $hour, $min);
  }

  if ($opt_s ){print $pname,"\n" if $is_a_file;}else{
    printf "%s %-10s %8s %s %2d %5s %s\n",
          $TSID,
                 $perms,
                     $sizemm,
                         $moname,
                            $mday,
                                $timeyear,
                                    $pname;

  }
  1;
}

sub sizemm {
    sprintf("%3d, %3d", ($rdev >> 8) & 255, $rdev & 255);
}
