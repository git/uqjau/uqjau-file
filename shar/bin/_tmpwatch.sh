#!/bin/bash
set -xu

flags=${tmpwatch_flags:--acm}
 days=${tmpwatch_days:-21}

/usr/sbin/tmpwatch --verbose "$flags" $(( 24 * $days )) "${@:-${HOME}/tmp}"
exit $?

    : <<\____eohd
    SYNOPSIS
        Shell wrapper for tmpwatch with different defaults.

    --man page snips--
     SYNOPSIS
            tmpwatch [-u|-m|-c] [-MUXadfqstvx] [--verbose] [--force] [--all]
                           [--nodirs] [--nosymlinks] [--test] [--fuser] [--quiet]
                           [--atime|--mtime|--ctime] [--dirmtime] [--exclude <path>]
                           [--exclude-user <user>] [--exclude-pattern <pattern>]
                           <hours> <dirs>
    --snip
          -a, --all
                  Remove all file types, not just regular  files,  symbolic  links
                  and directories.
    --snip
          -u, --atime
                 Make  the  decision  about deleting a file based on the file's atime (access
                 time). This is the default.
    
                 Note that the periodic updatedb file system scans keep the atime of directo-
                 ries recent.
    
          -m, --mtime
                 Make the decision about deleting a file based on the file's mtime (modifica-
                 tion time) instead of the atime.
    
          -c, --ctime
                 Make the decision about deleting a file based on  the  file's  ctime  (inode
                 change  time) instead of the atime; for directories, make the decision based
                 on the mtime.
    
     
    > IMP:
          If  the  --atime,  --ctime or --mtime options are used in combination, the decision
          about deleting a file will be based on the maximum of these times.  The  --dirmtime
          option implies ignoring atime of directories, even if the --atime option is used.
    
          -x, --exclude=path
                 Skip  path;  if  path  is a directory, all files contained in it are skipped
                 too.  If path does not exist, it must be an absolute path that  contains  no
                 symbolic links.
    
          -X, --exclude-pattern=pattern
                 Skip  paths matching pattern; if a directory matches pattern, all files con-
                 tained in it are skipped too.  pattern must match an absolute path that con-
                 tains no symbolic links.
          -f, --force
                 Remove files even if root doesn't have write access (akin to rm -f).
    
     NAME
            tmpwatch - removes files which haven't been accessed for a period of time
     
     SYNOPSIS
            tmpwatch [-u|-m|-c] [-MUXadfqstvx] [--verbose] [--force] [--all]
                           [--nodirs] [--nosymlinks] [--test] [--fuser] [--quiet]
                           [--atime|--mtime|--ctime] [--dirmtime] [--exclude <path>]
                           [--exclude-user <user>] [--exclude-pattern <pattern>]
                           <hours> <dirs>

____eohd
