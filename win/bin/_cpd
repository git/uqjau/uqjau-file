#!/usr/bin/env bash
set -ue

# --------------------------------------------------------------------
# Synopsis: Recursive file copy, using robocopy so windows
#   permissions result.  Attempts to behave like UNIX cp. (cygwin only)
# --------------------------------------------------------------------
# Usage: ourname DIR1 [DIR2 ...] DESTDIR
# --------------------------------------------------------------------
# Options: none
# --------------------------------------------------------------------

# ====================================================================
# RCS INFORMATION (do not touch fields bounded by $)
# --------------------------------------------------------------------
#     $Date: 2013/03/02 17:45:50 $   (GMT)
# $Revision: 1.11 $
#   $Author: rodmant $ (aka last changed by)
#   $Locker: rodmant $
#   $Source: /usr/local/7Rq/package/cur/file-2011.05.22/win/bin/RCS/_cpd._m4,v $
#      $Log: _cpd._m4,v $
#      Revision 1.11  2013/03/02 17:45:50  rodmant
#      *** empty log message ***
#
#      Revision 1.1  2013/01/21 03:38:15  rodmant
#      Initial revision
#
#
# --------------------------------------------------------------------

# ====================================================================
# Copyright (c) 2013 Tom Rodman <Rodman.T.S@gmail.com>
# --------------------------------------------------------------------

# == Software License ==
# This file is part of uqjau.
#
# uqjau is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# uqjau is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with uqjau.  If not, see <http://www.gnu.org/licenses/>.
# ---

# *****************************************************************
# define env var dir-defs for our "uqjau tools"
# *****************************************************************
cd ${0%/*}/../../
# $(dirname $0)
  # cd 2 dirs up from ( ${0%/*} == $(dirname $0) )
  # if  $0 has no "/"; 'source' below fails/we abort, that's OK (we do not support . in path)
_29r=$PWD
cd "$OLDPWD"

cd ${0%/*}
   # dirname of $0
_29rev=${PWD##*/}
   # Basename of dir below 'commands', 'scommands' or 'lib'; it contains the soft link
   # to this script.
cd "$OLDPWD"

if ! test -f $_29r/package/$_29rev/main/etc/dirdefs; then
  # create default dirdefs 
  source $_29r/package/$_29rev/main/slib/seed_dirdefs.shinc
else
  source $_29r/package/$_29rev/main/etc/dirdefs
    # not security risk if $0 is in correct commands dir
    # dirdefs appends $_29rev suffix to some defs
fi

#--

source $_29lib/bash_common.shinc
  # for summary of what is provided run this:
  #   sed -n '2,/-end-of-help/p' $_lib/bash_common.shinc|less


# ==================================================
# Process optional parameters.
# ==================================================

opt_true=1 OPTIND=1
  # OPTIND=1 only needed for 2nd and subsequent getopt invocations
  # since shell's start up sets it to 1

while getopts :fr:m: opt_char
do
   # Check for errors.
   # echo "opt_char:[$opt_char] OPTARG:[${OPTARG:-}] OPTIND:[$OPTIND]"
   #  $OPTIND is index of next arg to be processed (Ex: 1 == 1st arg)
   case $opt_char in
     \?) >&2 echo $ourname: unsupported option \(-$OPTARG\)
         # echo;help
         exit 3;
     ;;
     :) >&2 echo $ourname: missing argument for option \(-$OPTARG\)
         # Unfortunately this only works if the switch is last arg 
         # on command line.
         # echo;help
         exit 3;
     ;;
   esac

   case ${OPTARG:-} in
     -*)
       echo $ourname: [$OPTARG] not supported as arg to [-$opt_char]
       echo \ \ since it begins with -.
       # echo;help
       exit 3
     ;;
   esac

   # save info in an "OPT_*" env var.
   eval OPT_${opt_char}="\"\${OPTARG:-$opt_true}\""
done

shift $(( $OPTIND -1 ))
unset opt_true opt_char

_email_on_unexpected_exit=${_email_on_unexpected_exit:-yes}
_email_if_dying_on_trapped_sig=${_email_if_dying_on_trapped_sig:-no}
# _email_if_errlog_exists=yes # for Example "errlog foo" => e-mail

# =========================================================
# log creation and purging
# =========================================================
#log=$(mk_log_rotating -p 60 -d 1)
  # rotation: 60 periods of 1 day; log name prefix: {0..59}
#log=$(mk_log_expire_named -e 30)
  # logname contains a time to live string; -e 30 => expire after 30 days

# --------------------------------------------------------------------
#
#   read rcfile ( for env var defs );
#   local rcfile for local setting, else look for site setting
#
#   to see what dirs $_29eloc and $_29team are for:
#     awk '/## shared script rc files/, /^#-/' $_m/etc/dirdefs
#
# --------------------------------------------------------------------
for dir in $_29eloc $_29team
do
  rcfile=$dir/${ourname}rc
    # env vars impacted by rcfile:
    #   imp!! LIST_VARS_CHANGED_HERE
  if test -s "$rcfile"
  then
    source "$rcfile"
    break #prefer local file
  fi
done
unset rcfile dir
#--

_robocopy()
{
  #(set -x;robocopy /E /D:T /R:5 /NP "$@")
  local src=$1
  local dest=$2
  local retval

  local src_bn="$(basename "$src")"
  local robo_dest

  local dest_parent=$(dirname $dest)
  [[ -d $dest_parent ]] ||
  { echo "$FUNCNAME:ERROR:[$dest_parent] must exist" >&2; return 1; }

  if [[ $src_bn = . ]];then
    # FYI: basename of /a/s/d/. is "."
    #   $ echo $(basename /a/s/d/.) $(basename .)
    #   . .

    robo_dest=$dest
  else
    # src basename is not "." 
    if [[ -d $dest ]];then
      if [[ -d $dest/$src_bn ]];then
        robo_dest=$dest/$src_bn/$src_bn
      else
        robo_dest=$dest/$src_bn
      fi
    else
      robo_dest=$dest
        # implies a potential 'rename'
    fi
  fi

  [[ -d $robo_dest ]] || mkdir "$robo_dest"
  
  local       wsrc="$(cygpath -aw "$src"       )"
  local wrobo_dest="$(cygpath -aw "$robo_dest" )"
  if ( set -x;
   robocopy /E /W:5 /R:3 /NP /NC /NS /NFL /NDL /NJH /NJS \
     "$wsrc"  \
     "$wrobo_dest" 
     ) 
  then 
    retval=0
    : 0 => no copying done or required, trees are completely synchronized
  else
    retval=$?
  fi

  if [[ $retval = 1 || $retval = 0 || $retval = 3 ]];then return 0
  else
    echo "$ourname:$FUNCNAME:ERROR:robocopy returned [$retval]" >&2
    return 1
  fi
}

# ####################################################################
# Main procedure.
#   script start
# ####################################################################

# main
{

## allow multiple dirs to be copied to same dest
argc=$#
dest=${!argc}

## prepare list of source dirs in array ${src_files[@]}":
for (( i=1; $i < $argc ;i += 1));do src_files+=(${!i}) ;done

## all source args should be dirs
abort=""
for d in "${src_files[@]}";do
  if [[ ! -d "$d" ]] ; then
    echo "$ourname:ERROR:not a directory:["$d"]" >&2
    abort=1
  fi
done
[[ -z ${abort:-} ]] || exit 1

for s in "${src_files[@]}";do
  _robocopy "$s" "$dest" || exit $?
done

# ==================================================
# main done.
# ==================================================
: dropping through to main end #set -x makes this echo
exit 0

#main end
}

# + wget --cache=off --no-check-certificate -O - http://ss64.com/nt/robocopy.html
# --2013-01-29 20:01:11--  http://ss64.com/nt/robocopy.html
# --snip
#          ROBOCOPY.exe ([5]XP Resource Kit/Standard Windows 7 command)
# 
#    Robust File and Folder Copy.
#    By default Robocopy will only copy a file if the source and destination have
#    different time stamps or different file sizes.
# Syntax
#       ROBOCOPY Source_folder Destination_folder [files_to_copy] [options]
# 
# Key
#    file(s)_to_copy : A list of files or a [6]wildcard.
#                           (defaults to copying *.*)
# 
#   Source options
#                 /S : Copy Subfolders.
#                 /E : Copy Subfolders, including Empty Subfolders.
#  /COPY:copyflag[s] : What to COPY (default is /COPY:DAT)
#                       (copyflags : D=Data, A=Attributes, T=Timestamps
#                        S=Security=NTFS ACLs, O=Owner info, U=aUditing info).
#               /SEC : Copy files with SECurity (equivalent to /COPY:DATS).
#           /DCOPY:T : Copy Directory Timestamps. ##
#           /COPYALL : Copy ALL file info (equivalent to /COPY:DATSOU).
#            /NOCOPY : Copy NO file info (useful with /PURGE).
# 
#                 /A : Copy only files with the Archive attribute set.
#                 /M : like /A, but remove Archive attribute from source files.
#             /LEV:n : Only copy the top n LEVels of the source tree.
# 
#          /MAXAGE:n : MAXimum file AGE - exclude files older than n days/date.
#          /MINAGE:n : MINimum file AGE - exclude files newer than n days/date.
#                      (If n < 1900 then n = no of days, else n = YYYYMMDD date).
# 
#               /FFT : Assume FAT File Times (2-second date/time granularity).
#               /256 : Turn off very long path (> 256 characters) support.
# 
#    Copy options
#                 /L : List only - don’t copy, timestamp or delete any files.
#               /MOV : MOVe files (delete from source after copying).
#              /MOVE : Move files and dirs (delete from source after copying).
# 
#                 /Z : Copy files in restartable mode (survive network glitch).
#                 /B : Copy files in Backup mode.
#                /ZB : Use restartable mode; if access denied use Backup mode.
#             /IPG:n : Inter-Packet Gap (ms), to free bandwidth on slow lines.
# 
#               /R:n : Number of Retries on failed copies - default is 1 million.
#               /W:n : Wait time between retries - default is 30 seconds.
#               /REG : Save /R:n and /W:n in the Registry as default settings.
#               /TBD : Wait for sharenames To Be Defined (retry error 67).
# 
#    Destination options
# 
#     /A+:[RASHCNET] : Set file Attribute(s) on destination files + add.
#     /A-:[RASHCNET] : UnSet file Attribute(s) on destination files - remove.
#               /FAT : Create destination files using 8.3 FAT file names only.
# 
#            /CREATE : CREATE directory tree structure + zero-length files only.
#               /DST : Compensate for one-hour DST time differences ##
#             /PURGE : Delete dest files/folders that no longer exist in source.
#               /MIR : MIRror a directory tree - equivalent to /PURGE plus all su
# bfolders (/E)
# 
#    Logging options
#                 /L : List only - don’t copy, timestamp or delete any files.
#                /NP : No Progress - don’t display % copied.
#          /LOG:file : Output status to LOG file (overwrite existing log).
#       /UNILOG:file : Output status to Unicode Log file (overwrite) ##
#         /LOG+:file : Output status to LOG file (append to existing log).
#      /UNILOG+:file : Output status to Unicode Log file (append) ##
#                /TS : Include Source file Time Stamps in the output.
#                /FP : Include Full Pathname of files in the output.
#                /NS : No Size - don’t log file sizes.
#                /NC : No Class - don’t log file classes.
#               /NFL : No File List - don’t log file names.
#               /NDL : No Directory List - don’t log directory names.
#               /TEE : Output to console window, as well as the log file.
#               /NJH : No Job Header.
#               /NJS : No Job Summary.
# 
#  Repeated Copy Options
#             /MON:n : MONitor source; run again when more than n changes seen.
#             /MOT:m : MOnitor source; run again in m minutes Time, if changed.
# 
#      /RH:hhmm-hhmm : Run Hours - times when new copies may be started.
#                /PF : Check run hours on a Per File (not per pass) basis.
# 
#  Job Options
#       /JOB:jobname : Take parameters from the named JOB file.
#      /SAVE:jobname : SAVE parameters to the named job file
#              /QUIT : QUIT after processing command line (to view parameters).
#              /NOSD : NO Source Directory is specified.
#              /NODD : NO Destination Directory is specified.
#                /IF : Include the following Files.
# 
# Advanced options you'll probably never use
#            /EFSRAW : Copy any encrypted files using EFS RAW mode. ##
#            /MT[:n] : Multithreaded copying, n = no. of threads to use (1-128) #
# ##
#                      default = 8 threads, not compatible with /IPG and /EFSRAW
#                      The use of /LOG is recommended for better performance.
# 
#            /SECFIX : FIX [7]file SECurity on all files, even skipped files.
#            /TIMFIX : FIX file TIMes on all files, even skipped files.
# 
#                /XO : eXclude Older - if destination file exists and is the same
#  date
#                      or newer than the source - don’t bother to overwrite it.
#          /XC | /XN : eXclude Changed | Newer files
#                /XL : eXclude "Lonely" files and dirs (present in source but not
#  destination)
#                      This will prevent any new files being added to the destina
# tion.
#                /XX : eXclude "eXtra" files and dirs (present in destination but
#  not source)
#                      This will prevent any deletions from the destination. (thi
# s is the default)
# 
# /XF file [file]... : eXclude Files matching given names/paths/wildcards.
# /XD dirs [dirs]... : eXclude Directories matching given names/paths.
#                      XF and XD can be used in combination  e.g.
#                      ROBOCOPY c:\source d:\dest /XF *.doc *.xls /XD c:\unwanted
#  /S
# 
#    /IA:[RASHCNETO] : Include files with any of the given Attributes
#    /XA:[RASHCNETO] : eXclude files with any of the given Attributes
#                /IS : Include Same, overwrite files even if they are already the
#  same.
#                /IT : Include Tweaked files.
#                /XJ : eXclude Junction points. (normally included by default).
# 
#             /MAX:n : MAXimum file size - exclude files bigger than n bytes.
#             /MIN:n : MINimum file size - exclude files smaller than n bytes.
#          /MAXLAD:n : MAXimum Last Access Date - exclude files unused since n.
#          /MINLAD:n : MINimum Last Access Date - exclude files used since n.
#                      (If n < 1900 then n = n days, else n = YYYYMMDD date).
# 
#             /BYTES : Print sizes as bytes.
#                 /X : Report all eXtra files, not just those selected & copied.
#                 /V : Produce Verbose output log, showing skipped files.
#               /ETA : Show Estimated Time of Arrival of copied files.
# 
#    ## = New Option in Vista (XP027) all other options are valid for the XP
#    version of Robocopy (XP010)
#    ### = New Option in Windows 7 and Windows 2008 R2
# 
#    Robocopy [8]EXIT CODES
# 
#    File Attributes [RASHCNETO]
#  R – Read only
#  A – Archive
#  S – System
#  H – Hidden
#  C – Compressed
#  N – Not content indexed
#  E – Encrypted
#  T – Temporary
#  O - Offline
# 
#    If either the source or desination are a "quoted long foldername" do not
#    include a trailing backslash as this will be treated as an escape character,
#    i.e. "C:\some path\" will fail but "C:\some path\\" or "C:\some path\." or
#    "C:\some path" will work.
# 
#    By copying only the files that have changed, robocopy can be used to backup
#    very large volumes.
# 
#    ROBOCOPY  will  accept  UNC pathnames including UNC pathnames over 256
#    characters long.
# 
#    /REG Writes to the registry at HKCU\Software\Microsoft\ResKit\Robocopy
# 
#    /XX (exclude extra) If used in conjunction with /Purge or /Mir, this switch
#    will  take  precedence  and  prevent  any files being deleted from the
#    destination.
# 
#    To limit the network bandwidth used by robocopy, specify the Inter-Packet
#    Gap parameter /IPG:n
#    This will send packets of 64 KB each followed by a delay of n Milliseconds.
# 
# Open Files
# 
#    Robocopy  will  fail  to  copy files that are locked by other users or
#    applications, so limiting the number of retries with /R:0 will speed up
#    copying by skipping any in-use files. The Windows Volume Shadow Copy service
#    is the only Windows subsystem that can copy open files. Robocopy does not
#    use the Volume Shadow Copy service, but it can backup a volume shadow that
#    has already been created with VSHADOW or [9]DISKSHADOW.
# 
# Permissions
# 
#    All  versions  of  Robocopy  will copy security information (ACLs) for
#    directories, version XP010 will [10]not copy file security changes unless
#    the file itself has also changed, this greatly improves performance.
# 
#    /B (backup mode) will allow Robocopy to override file and folder permission
#    settings (ACLs).
# 
#    ERROR 5 (0x00000005) Changing File Attributes ... Access is denied
#    This error usually means that File/Folder permissions or Share permissions
#    on either the source or the destination are preventing the copy, either
#    change the permissions or run the command in backup mode with /B.
# 
#    To run ROBOCOPY under a non-administrator account will require [11]backup
#    files[12] privilege, to copy security information [13]auditing[14] privilege
#    is also required, plus of course you need at least read access to the files
#    and folders.
# 
# Availability
# 
#    Robocopy is a standard command in Windows 7 and above. The [15]Windows
#    Server 2003 Resource Kit Tools include Robocopy XP010, which can be run on
#    NT  4/  Windows  2000. Robocopy does not run on Windows 95, or NT 3.5.
#    (RoboCopy is a Unicode application).
# 
#    Robocopy 'Jobs' and the 'MOnitor source' option provide an alternative to
#    setting up a [16]Scheduled Task to run a batchfile with a RoboCopy command.
# 
# Examples:
# 
#    Copy files from one server to another including subfolders (/S)
#    If this command is run repeatedly it will skip any files already in the
#    destination, however it is not a true mirror as any files deleted from the
#    source will remain in the destination.
# ROBOCOPY \\Server1\reports \\Server2\backup *.doc /S
# 
#    List files over 32 MBytes in size:
# ROBOCOPY C:\work /MAX:33554432 /L
# 
#    Move files over 14 days old: (note the MOVE option will fail if any files
#    are open and locked.)
# ROBOCOPY C:\work C:\destination /move /minage:14
# 
#    Backup a Server:
#    The script below copies data from FileServ1 to FileServ2, the destination
#    holds a full mirror along with file security info. When run regularly to
#    synchronize the source and destination, robocopy will only copy those files
#    that have changed (change in time stamp or size.)
# 
#    @ECHO OFF
#    SETLOCAL
# 
#    SET _source=\\FileServ1\e$\users
# 
#    SET _dest=\\FileServ2\e$\BackupUsers
# 
#    SET _what=/COPYALL /B /SEC /MIR
#    :: /COPYALL :: COPY ALL file info
#    :: /B :: copy files in Backup mode.
#    :: /SEC :: copy files with SECurity
#    :: /MIR :: MIRror a directory tree
#    SET _options=/R:0 /W:0 /LOG:MyLogfile.txt /NFL /NDL
#    :: /R:n :: number of Retries
#    :: /W:n :: Wait time between retries
#    :: /LOG :: Output log file
#    :: /NFL :: No file logging
#    :: /NDL :: No dir logging
#    ROBOCOPY %_source% %_dest% %_what% %_options%
# 
#    Run two robocopy jobs at the same time with [17]START /Min
# Start /Min "Job one" Robocopy \\FileServA\C$\Database1 \\FileServeBackupA\c$\Ba
# ckups
# Start /Min "Job two" Robocopy \\FileServB\C$\Database2 \\FileServeBackupB\c$\Ba
# ckups
# 
#    Bugs
#    Version XP026 returns a success errorlevel even when it fails.
# 
#    “One, a robot may not injure a human being, or through inaction, allow a
#    human being to come to harm” - Isaac Asimov, Laws of Robotics from I. Robot,
#    1950
# 
#    Related:
#    [18]Robocopy EXIT CODES
#    [19]DiskShadow - Copy open files (Shadow copies)
#    [20]COPY - Copy one or more files to another location
#    [21]Robocopy GUI - Technet magazine (installs Robocopy [22]XP026)
#    [23]RichCopy free GUI copy utility - Ken Tamaru @ Microsoft
#    [24]SyncToy - Microsoft Powertoy for synchronizing two folders
#    [25]Convert KB/MB - Bits and Bytes, bandwidth calculations
#    Permcopy - Copy share & file ACLs from one share to another. ([26]Win 2K
#    ResKit)
#    [27]Q323275 - Copy Security info without copying files (/SECFIX or /COPY:S)
#    Equivalent bash command:[28]rsync - Remote file copy (Synchronize file
#    trees)
# --snip
# + wget --cache=off --no-check-certificate -O - http://ss64.com/nt/robocopy-exit.html
# --2013-02-02 14:51:54--  http://ss64.com/nt/robocopy-exit.html
# Resolving ss64.com... + lynx -dump -stdin
# 216.92.29.160
# Connecting to ss64.com|216.92.29.160|:80... connected.
# HTTP request sent, awaiting response... 200 OK
# Length: 5387 (5.3K) [text/html]
# Saving to: `STDOUT'
# --snip
# 
#                             [5]ROBOCOPY Exit Codes
# 
#    The return code from Robocopy is a bit map, defined as follows:
#     Hex   Decimal  Meaning if set
#     010  16       Serious error. Robocopy did not copy any files.
#                    Either a usage error or an error due to insufficient access privileges
#                    on the source or destination directories.
# 
#     008   8       Some files or directories could not be copied
#                    (copy errors occurred and the retry limit was exceeded).
#                    Check these errors further.
# 
#     004   4       Some Mismatched files or directories were detected.
#                    Examine the output log. Some housekeeping may be needed.
# 
#     002   2       Some Extra files or directories were detected.
#                    Examine the output log for details.
# 
#     001   1       One or more files were copied successfully (that is, new files have arrived).
# 
#     000   0       No errors occurred, and no copying was done.
#                    The source and destination directory trees are completely synchronized.
# 
#    You can use this in a batch file to report anomalies, as follows:
#     if errorlevel 16 echo ***FATAL ERROR*** & goto end
#     if errorlevel 15 echo OKCOPY + FAIL + MISMATCHES + XTRA & goto end
#     if errorlevel 14 echo FAIL + MISMATCHES + XTRA & goto end
#     if errorlevel 13 echo OKCOPY + FAIL + MISMATCHES & goto end
#     if errorlevel 12 echo FAIL + MISMATCHES& goto end
#     if errorlevel 11 echo OKCOPY + FAIL + XTRA & goto end
#     if errorlevel 10 echo FAIL + XTRA & goto end
#     if errorlevel 9 echo OKCOPY + FAIL & goto end
#     if errorlevel 8 echo FAIL & goto end
#     if errorlevel 7 echo OKCOPY + MISMATCHES + XTRA & goto end
#     if errorlevel 6 echo MISMATCHES + XTRA & goto end
#     if errorlevel 5 echo OKCOPY + MISMATCHES & goto end
#     if errorlevel 4 echo MISMATCHES & goto end
#     if errorlevel 3 echo OKCOPY + XTRA & goto end
#     if errorlevel 2 echo XTRA & goto end
#     if errorlevel 1 echo OKCOPY & goto end
#     if errorlevel 0 echo No Change & goto end
#     :end
# 
#    Example:
# 
#    Copy files from one server to another
# ROBOCOPY \\Server1\reports \\Server2\backup *.*
# if errorlevel 8 GOTO sub_fail
# if errorlevel 4 GOTO sub_housekeeping
# if errorlevel 1 GOTO :eof
# goto :eof
# 
# :sub_fail
# echo Something failed & goto :eof
# :sub_housekeeping
# echo Some housekeeping may be needed! & goto :eof
# 
#    Bugs
#    Version XP026 returns a success errorlevel even when it fails.
# 
#    Few men of action have been able to make a graceful exit at the appropriate
#    time ~ Malcolm Muggeridge
# 
# --snip
