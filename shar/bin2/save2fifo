#!/usr/bin/env bash
set -ue

# --------------------------------------------------------------------
# Synopsis: Make a fifo (currently in a fixed location), which
# creates a new temp file (to a currently fixed subdir ) each time
# a write to this fifo is completed.
# --------------------------------------------------------------------
# Usage: 
#   ex  ( The fifo:    ~/s2f    )

#     ~ $ batch <<<s2f   # "s2f" runs in background.
#     job 260 at 2013-10-06 10:13
#     ~ $ seq 50 > ~/s2f
#     ~ $ cd ~/tmp/s2f/
#     ~/tmp/s2f $ find . -type f -mmin -5 -ls
#     3768362    0 -rw-------   1 rodmant  staff           0 Oct  6 10:13 ./32053
#     3768361    4 -rw-------   1 rodmant  staff         141 Oct  6 10:13 ./32047
#     ~/tmp/s2f $ tail -3 32047
#     48
#     49
#     50
#
# Short name: s2f ( symbolic link in _29r commands dir )
# --------------------------------------------------------------------
# Bugs: only exits on SIGKILL
# --------------------------------------------------------------------
# Rating: tone: tool used: yes stable: y interesting: y TBD: y 
# --------------------------------------------------------------------
# TBD: clarify documentation, add some options

# ====================================================================
# RCS INFORMATION (do not touch fields bounded by $)
# --------------------------------------------------------------------
#     $Date: 2013/01/06 21:08:34 $   (GMT)
# $Revision: 1.2 $
#   $Author: rodmant $ (aka last changed by)
#   $Locker: rodmant $
#   $Source: /usr/local/7Rq/package/cur/file-2011.05.22/shar/bin2/RCS/save2fifo._m4,v $
#      $Log: save2fifo._m4,v $
#      Revision 1.2  2013/01/06 21:08:34  rodmant
#      *** empty log message ***
#
# --------------------------------------------------------------------

# ====================================================================
# Copyright (c) 2013 Tom Rodman <Rodman.T.S@gmail.com>
# --------------------------------------------------------------------

# == Software License ==
# This file is part of uqjau.
#
# uqjau is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# uqjau is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with uqjau.  If not, see <http://www.gnu.org/licenses/>.
# ---

# *****************************************************************
# define env var dir-defs for our "uqjau tools"
# *****************************************************************
cd ${0%/*}/../../
# $(dirname $0)
  # cd 2 dirs up from ( ${0%/*} == $(dirname $0) )
  # if  $0 has no "/"; 'source' below fails/we abort, that's OK (we do not support . in path)
_29r=$PWD
cd "$OLDPWD"

cd ${0%/*}
   # dirname of $0
_29rev=${PWD##*/}
   # Basename of dir below 'commands', 'scommands' or 'lib'; it contains the soft link
   # to this script.
cd "$OLDPWD"

if ! test -f $_29r/package/$_29rev/main/etc/dirdefs; then
  # create default dirdefs 
  source $_29r/package/$_29rev/main/slib/seed_dirdefs.shinc
else
  source $_29r/package/$_29rev/main/etc/dirdefs
    # not security risk if $0 is in correct commands dir
    # dirdefs appends $_29rev suffix to some defs
fi

#--

source $_29lib/bash_common.shinc
  # for summary of what is provided run this:
  #   sed -n '2,/-end-of-help/p' $_lib/bash_common.shinc|less

#--
_cleanup()
{
  rm -f $fifo
}

# ####################################################################
# Main procedure.
#   script start
# ####################################################################

# main
{

saves_dir=~/tmp/$ourname
fifodir=~
fifo=$fifodir/s2f
mkdir -p $fifodir $saves_dir

lock_file=/tmp/$ourname.pid
if [[ -s $lock_file ]] && kill -s 0 $(cat $lock_file) 2>/dev/null ;then
  echo $ourname:ERROR:$ourname:already running w/pid: $(cat $lock_file) >&2
  exit 1
fi

_cleanup
echo $$ > $lock_file

mkfifo $fifo

i=0
while :;do
  let i++ || :
  : $i =========================================================
  date
  echo starting cat
  : $i =========================================================
  tmpf=$(mkdir -p $saves_dir && mktemp $saves_dir/XXXXX)
  cat < $fifo > $tmpf || echo cat retval: $?
    # TBD: explain why cat does not show up in ps, is the fifo only invoking cat when STDIN ends?
    # TBD: explain why $ourname can only be killed with SIGKILL
  sleep 3
  :
done

# ==================================================
# main done.
# ==================================================
: dropping through to main end #set -x makes this echo
exit 0

#main end
}
